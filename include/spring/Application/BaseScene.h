#pragma once
#include <spring\Framework\IScene.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>

namespace Spring
{
	class  BaseScene : public IScene
	{
		Q_OBJECT

	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();

		void createGUI();

	private:
		QWidget *centralWidget;
		QGridLayout *gridLayout;
		QCustomPlot *customPlot;
		QHBoxLayout *horizontalLayout;
		QSpacerItem *horizontalSpacer;
		QPushButton *startButton;
		QPushButton *stopButton;
		QPushButton *backButton;

		private slots:
		void mf_backButton();
		
	};

}
