#pragma once
#include <spring\Framework\IScene.h>
#include <QWidget>

namespace Spring {
	class TrivialScene : public IScene
	{
	public:

		explicit TrivialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~TrivialScene();

	private:
		QWidget *centralWidget;
	};
}