#pragma once

class __declspec(dllexport) MyLibrary
{
public:
	static unsigned int computeNumberOfSamples(const unsigned int SampleRate, const double DisplayTime);
};