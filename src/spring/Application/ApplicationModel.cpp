#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>
#include <spring\Application\TrivialScene.h>

const std::string initialSceneName = "InitialScene";
const std::string baseSceneName = "BaseScene";
const std::string trivialSceneName = "TrivialScene";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{
	}

	void ApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);

		IScene* baseScene = new BaseScene(baseSceneName);
		m_Scenes.emplace(baseSceneName, baseScene);

		IScene* trivialScene = new TrivialScene(trivialSceneName);
		m_Scenes.emplace(trivialSceneName, trivialScene);
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		//add initial values for all transient data 

		std::ifstream fin("TransientData.txt");

		if (fin.good())
		{
			while (!fin.eof())
			{
				std::string key;
				fin >> key;

				std::string data;
				fin >> data;

				unsigned int intdata = atoi(data.c_str());				
				if (intdata)
				{
					m_TransientData.emplace(key, (unsigned int)intdata);
					continue;
				}

				double doubledata = atof(data.c_str());
				if (doubledata)
				{
					m_TransientData.emplace(key, doubledata);
					continue;
				}

				m_TransientData.emplace(key, data);
			}
		}
		else
		{
			std::string applicationName = "Undefined";
			m_TransientData.emplace("ApplicationName", applicationName);

			unsigned int sampleRate = 25600;
			m_TransientData.emplace("SampleRate", sampleRate);

			double displayTime = 0.1;
			m_TransientData.emplace("DisplayTime", displayTime);

			unsigned int refreshRate = 1;
			m_TransientData.emplace("RefreshRate", refreshRate);
		}
		fin.close();
	}

	ApplicationModel::~ApplicationModel()
	{
		std::ofstream fout("TransientData.txt");

		for (const auto& pair : m_TransientData)
		{
			fout << pair.first << " ";

			if (pair.second.type() == typeid(unsigned int))
			{
				unsigned int data = boost::any_cast<unsigned>(pair.second);
				fout << data;
			}
			else if (pair.second.type() == typeid(double))
			{
				double data = boost::any_cast<double>(pair.second);
				fout << data;
			}
			else if (pair.second.type() == typeid(std::string))
			{
				std::string data = boost::any_cast<std::string>(pair.second);
				fout << data;
			}

			fout << std::endl;
		}

		fout.close();
	}
}
