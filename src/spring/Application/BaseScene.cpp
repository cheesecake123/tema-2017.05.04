#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));

		createGUI();

		QObject::connect(backButton, SIGNAL(released()), this, SLOT(mf_backButton()));
	}

	void BaseScene::release()
	{
	}
	
	BaseScene::~BaseScene()
	{
	}
	void BaseScene::createGUI()
	{
		if (m_uMainWindow->objectName().isEmpty())
			m_uMainWindow->setObjectName(QStringLiteral("MainWindow"));
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		customPlot = new QCustomPlot(centralWidget);
		customPlot->setObjectName(QStringLiteral("customPlot"));

		gridLayout->addWidget(customPlot, 1, 1, 1, 1);

		horizontalLayout = new QHBoxLayout();
		horizontalLayout->setSpacing(6);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout->addItem(horizontalSpacer);

		startButton = new QPushButton(centralWidget);
		startButton->setObjectName(QStringLiteral("startButton"));

		horizontalLayout->addWidget(startButton);

		stopButton = new QPushButton(centralWidget);
		stopButton->setObjectName(QStringLiteral("stopButton"));

		horizontalLayout->addWidget(stopButton);

		backButton = new QPushButton(centralWidget);
		backButton->setObjectName(QStringLiteral("backButton"));

		horizontalLayout->addWidget(backButton);


		gridLayout->addLayout(horizontalLayout, 2, 1, 1, 1);

		m_uMainWindow->setCentralWidget(centralWidget);

		startButton->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
		stopButton->setText(QApplication::translate("MainWindow", "Stop", Q_NULLPTR));
		backButton->setText(QApplication::translate("MainWindow", "Back" , Q_NULLPTR));
	}

	void BaseScene::mf_backButton()
	{
		const std::string windowTitle = "Spring school project";
		m_uMainWindow->setWindowTitle(windowTitle.c_str());

		const std::string c_szNextSceneName = "InitialScene";
		emit SceneChange(c_szNextSceneName);
	}
}
