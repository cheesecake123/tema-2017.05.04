#include "..\..\..\include\spring\Application\TrivialScene.h"
#include <mylibrary.h>
#include <QLabel>

namespace Spring 
{
	TrivialScene::TrivialScene(const std::string & ac_szSceneName):IScene(ac_szSceneName)
	{
	}

	void TrivialScene::createScene()
	{
		unsigned int sampleRate = boost::any_cast<unsigned>(m_TransientDataCollection["SampleRate"]);

		double displayTime = boost::any_cast<double>(m_TransientDataCollection["DisplayTime"]);

		unsigned int numberOfSamples = MyLibrary::computeNumberOfSamples(sampleRate, displayTime);


		centralWidget = new QWidget(m_uMainWindow.get());

		QLabel *output = new QLabel(centralWidget);

		output->setText(QString::number(numberOfSamples));

		m_uMainWindow->setCentralWidget(centralWidget);
	}

	void TrivialScene::release()
	{
		delete centralWidget;
	}

	TrivialScene::~TrivialScene()
	{
	}

}